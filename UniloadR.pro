#-------------------------------------------------
#
# Project created by QtCreator 2015-02-14T22:32:27
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    UniloadR
TEMPLATE =  app

#LIBS           = -Lplugins
#LIBS += -L$$PWD/plugins/ -lPomfse

SOURCES +=  main.cpp\
            mainwindow.cpp

HEADERS  += mainwindow.h \
            UploaderInterface.h

FORMS    += mainwindow.ui

#VERSION =  MAJOR.MINOR.REVISION.HOTFIX
VERSION =   0.4.1.1
