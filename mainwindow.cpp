#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    statusBar = new QProgressBar;
    ui->statusBar->addPermanentWidget(statusBar, 0);

    signalMapper = new QSignalMapper(this);

    uploadTime.start();

    loadPlugins();

    connect(ui->statusBar,  SIGNAL(messageChanged(QString)),
            this,           SLOT(showAppVersion(QString)));

    showAppVersion("");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_bSelect_clicked()
{
    if(ui->cFileHost->currentIndex() == -1)
    {
        qDebug() << "Error: No file host selected";
        return;
    }

    QFileDialog browseDialog(this, "Select a file to upload");
    browseDialog.setFileMode(QFileDialog::ExistingFile);
    browseDialog.setViewMode(QFileDialog::Detail);
    browseDialog.setOption(QFileDialog::ReadOnly);
    QString filePath;
    if(browseDialog.exec())
        filePath = browseDialog.selectedFiles().first();
    else
        return; //TODO Add a proper error dialog or log.

    ui->lEFilePath->setText(filePath);

    ui->cFileHost->currentIndex();
    UploaderInterface *currentUploader = UploaderList.at(ui->cFileHost->currentIndex());
    currentUploader->uploadFile(filePath);
    uploadTime.restart();

    connect(dynamic_cast<QObject*>(currentUploader),    SIGNAL(uploadFinished(QString)),
            this,                                       SLOT(receiveFileUrl(QString)));

    connect(dynamic_cast<QObject*>(currentUploader),    SIGNAL(textError(QString)),
            this,                                       SLOT(displayError(QString)));

    connect(dynamic_cast<QObject*>(currentUploader),    SIGNAL(uploadError(QNetworkReply::NetworkError)),
            this,                                       SLOT(uploadError(QNetworkReply::NetworkError)));

    connect(dynamic_cast<QObject*>(currentUploader),    SIGNAL(uploadProgress(qint64,qint64)),
            this,                                       SLOT(uploadProgress(qint64,qint64)));
}

void MainWindow::showAppVersion(QString oldMessage)
{
    if(oldMessage.isEmpty())
        ui->statusBar->showMessage("UniloadR_0.4.1.1");
    qDebug() << "Showing message...";
}

void MainWindow::receiveFileUrl(QString fileUrl)
{
    ui->statusBar->showMessage("");
    ui->lEFileUrl->setText(fileUrl);
}

void MainWindow::displayError(QString error)
{
    QMessageBox::critical(this, "Error during upload", error, QMessageBox::Ok);
}

void MainWindow::uploadProgress(qint64 sentBytes, qint64 totalBytes)
{
    //TODO Change from progressBar to statusBar...
    if(totalBytes <= 0)
        return;

    double speed = sentBytes * 1000.0 / uploadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024*1024;
        unit = "MB/s";
    }

    ui->statusBar->showMessage(QString::fromLatin1("U: %1 %2").arg(speed, 3, 'f', 1).arg(unit));
    statusBar->setValue(((int)sentBytes * 100) / (int)totalBytes);
}

void MainWindow::uploadError(QNetworkReply::NetworkError error)
{
    QMessageBox::critical(this, "Error during upload", QString::number(error), QMessageBox::Ok);
}

void MainWindow::loadPlugins()
{
    pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");

    ui->menuFile_hosts->clear();
    int i = 0;

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) {
            qDebug() << "Loaded plugin file " << fileName << ".";
            UploaderInterface *iUploader = qobject_cast<UploaderInterface *>(plugin);
            if(iUploader)
            {
                //TODO Implement refreshing of the plugins
                //IMPL Clear UploaderList and relaunch function
                qDebug() << "Adding plugin #" << i;
                QAction *pluginAction = ui->menuFile_hosts->addAction(iUploader->getSiteName(), signalMapper, SLOT(map()));
                signalMapper->setMapping(ui->menuFile_hosts->actions().at(i), i);
                pluginAction->setToolTip(iUploader->getSiteDescription() + "\n\n" + iUploader->getSiteUrl());
                UploaderList.append(iUploader);
                ui->cFileHost->addItem(iUploader->getSiteName());
                i++;
            }
            pluginFileNames += fileName;
        }
        else
            qDebug() << "File " << fileName << "is not a plugin file.";
    }

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(selectPlugin(int)));
}

void MainWindow::selectPlugin(int i)
{
    qDebug() << "Selecting plugin" << i;
    ui->cFileHost->setCurrentIndex(i);
}
