#ifndef UPLOADERINTERFACE_H
#define UPLOADERINTERFACE_H

#include <QtPlugin>

//needed classes
class QString;
class QFile;
//class QNetworkReply;
#include <QtNetwork>

class UploaderInterface
{
public:
    virtual ~UploaderInterface() {}

    virtual void uploadFile(QString const& filePath) = 0;
    //TODO Add support to upload directly from a QFile*
    //IMPL virtual void uploadFile(QFile* file) = 0;

    //TODO Add the options winddow's implementation directly in the plugin (maybe via another plugin?)
    //IMPL virtual void selectOptions(QObject *parentWindow); (parentwindow for modal)
    //IMPL or use blabla->setParent(this) instead in the mainwindow.

    virtual QString getSiteName() const = 0;
    virtual QString getSiteUrl() const = 0;
    virtual QString getSiteDescription() const = 0;
    //TODO Upgrade to UploaderInterface/1.1 and add APIKey parameter support
    //IMPL virtual QString getApiKeyHelp() const = 0;

    //IMPL virtual bool hasAPIKey() const = 0;

signals:
    virtual void textError(QString error) = 0;
    virtual void uploadError(QNetworkReply::NetworkError error) = 0;
    //TODO Maybe save the fileUrl in a variable so i can get it again later. QStringList containing all the uploads
    //IMPL Do that in mainwindow file
    virtual void uploadFinished(QString fileUrl) = 0;
    virtual void uploadProgress(qint64 sentBytes, qint64 totalBytes) = 0;
};

Q_DECLARE_INTERFACE(UploaderInterface, "org.UniloadR.UploaderInterface/1.0")

#endif // UPLOADERINTERFACE_H
