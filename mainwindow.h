#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QtNetwork>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QProgressBar>
#include <QSignalMapper>

#include "UploaderInterface.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_bSelect_clicked();
    void showAppVersion(QString oldMessage);

    void receiveFileUrl(QString fileUrl);
    void displayError(QString error);
    void uploadProgress(qint64 sentBytes, qint64 totalBytes);
    void uploadError(QNetworkReply::NetworkError error);

    void selectPlugin(int i);

private:
    Ui::MainWindow *ui;
    QProgressBar *statusBar;
    QSignalMapper *signalMapper;

    QDir pluginsDir;
    QStringList pluginFileNames;
    QList<UploaderInterface*> UploaderList;

    QTime uploadTime;

    void loadPlugins();
};

#endif // MAINWINDOW_H
